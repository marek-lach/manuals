# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2023, Inkscape Documentation Authors
# This file is distributed under the same license as the Inkscape Beginners'
# Guide package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Inkscape Beginners' Guide 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-11 16:48+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: zh_TW\n"
"Language-Team: zh_TW <LL@li.org>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: ../../source/cloning-objects.rst:3
msgid "Cloning Objects"
msgstr ""

#: ../../source/cloning-objects.rst:5
msgid ""
"|Large create clone icon| :kbd:`Alt` + :kbd:`D` :menuselection:`Edit --> "
"Clone --> Create Clone`"
msgstr ""

#: ../../source/cloning-objects.rst:33
msgid "Large create clone icon"
msgstr ""

#: ../../source/cloning-objects.rst:7
msgid ""
"You can save yourself some time when you know how to use :term:`clones "
"<Clone>`. With :kbd:`Alt` + :kbd:`D`, a clone can be created just as "
"quickly as a copy or a duplicate. One only needs to remember that the "
"option exists."
msgstr ""

#: ../../source/cloning-objects.rst:11
msgid ""
"The new clone will appear right on top of the original, and will follow "
"all modifications made on the original, no matter if they affect its "
"style (stroke, fill,...) or its shape."
msgstr ""

#: ../../source/cloning-objects.rst:15
msgid ""
"The clone's shape cannot be edited directly. Its colors can only be "
"changed if the original has no color itself. Its size and rotation can be"
" changed freely."
msgstr ""

#: ../../source/cloning-objects.rst:18
msgid ""
"To remove a color from an object, you must 'unset' its paint, using the "
"question mark button |Unset color icon| in the :guilabel:`Fill and "
"Stroke` dialog."
msgstr ""

#: ../../source/cloning-objects.rst:35
msgid "Unset color icon"
msgstr ""

#: ../../source/cloning-objects.rst:26
msgid "The left, selected object is a clone of the right object."
msgstr ""

#: ../../source/cloning-objects.rst:26
msgid ""
"All these stars are clones of the star in the top left corner. They have "
"the same color and shape, but different sizes and rotations. Note that "
"the selected clone does not have any star tool handles."
msgstr ""

#: ../../source/cloning-objects.rst:28
msgid ""
"To turn a clone into an independent, fully editable object, you need to "
"**unlink** it from its original with :kbd:`Shift` + :kbd:`Alt` + "
":kbd:`D`, :menuselection:`Edit --> Clone --> Unlink Clone` or by clicking"
" on the corresponding icon |Unlink clone icon| in the command bar."
msgstr ""

#: ../../source/cloning-objects.rst:37
msgid "Unlink clone icon"
msgstr ""

#: ../../source/cloning-objects.rst:31
msgid ""
"To select the original of a clone, you can press :kbd:`Shift` + :kbd:`D` "
"or use :menuselection:`Edit --> Clone --> Select Original` in the menu."
msgstr ""

