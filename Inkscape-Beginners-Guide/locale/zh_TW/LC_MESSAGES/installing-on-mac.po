# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2023, Inkscape Documentation Authors
# This file is distributed under the same license as the Inkscape Beginners'
# Guide package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Inkscape Beginners' Guide 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-11 16:48+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: zh_TW\n"
"Language-Team: zh_TW <LL@li.org>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: ../../source/installing-on-mac.rst:3
msgid "Installing Inkscape on a Mac"
msgstr ""

#: ../../source/installing-on-mac.rst:5
msgid ""
"The current version of Inkscape can be installed on computers running "
"macOS version 10.11 or newer."
msgstr ""

#: ../../source/installing-on-mac.rst:8
msgid ""
"If you are on macOS 10.7 - 10.10, you need to install the older Inkscape "
"0.92.2 (from 2017) using the :ref:`separate instructions below "
"<inkscape092mac>`. Inkscape is also available for Mac users who prefer to"
" work with the `Homebrew and MacPorts`_ environments."
msgstr ""

#: ../../source/installing-on-mac.rst:15
msgid "Installing Inkscape on macOS 10.11 and newer"
msgstr ""

#: ../../source/installing-on-mac.rst:17
msgid ""
"This is the recommended method for most Mac users to install Inkscape. It"
" installs the current version of Inkscape and requires that your computer"
" is running macOS version 10.11 or newer."
msgstr ""

#: ../../source/installing-on-mac.rst:22
msgid ""
"Using a web browser, go to the `Inkscape downloads page "
"<https://inkscape.org/release/>`_."
msgstr ""

#: ../../source/installing-on-mac.rst:28
msgid ""
"Click the box labelled :guilabel:`macOS (10.11-10.15)` to download the "
"Inkscape disk image (:term:`DMG <Disk Image (.dmg)>`) file."
msgstr ""

#: ../../source/installing-on-mac.rst:34
msgid ""
"Once the download has finished, open your :file:`Downloads` folder in the"
" Finder. You can open this folder by selecting :menuselection:`Go --> "
"Downloads` from the menu bar in the Finder. Double-click the Inkscape DMG"
" file to open it."
msgstr ""

#: ../../source/installing-on-mac.rst:43 ../../source/installing-on-mac.rst:118
msgid ""
"Click and drag the Inkscape icon to the Applications icon as instructed "
"in this window. This will install Inkscape on your Mac."
msgstr ""

#: ../../source/installing-on-mac.rst:49 ../../source/installing-on-mac.rst:128
msgid ""
"Open Inkscape by double-clicking its icon in the :guilabel:`Applications`"
" folder. You can open the :guilabel:`Applications` folder by selecting "
":menuselection:`Go --> Applications` from the menu bar in the Finder."
msgstr ""

#: ../../source/installing-on-mac.rst:61
msgid "Installing Inkscape on OS X 10.7 - 10.10"
msgstr ""

#: ../../source/installing-on-mac.rst:63
msgid ""
"If you have an older Mac running OS X version 10.7 - 10.10, you can still"
" install an older version of Inkscape, v 0.92. As part of this process, "
"you will first install a helper program called :term:`XQuartz` and then "
"Inkscape 0.92 itself."
msgstr ""

#: ../../source/installing-on-mac.rst:70
msgid "Step 1: Install XQuartz"
msgstr ""

#: ../../source/installing-on-mac.rst:72
msgid ""
"Using a web browser, go to the `XQuartz website "
"<https://www.xquartz.org/>`_ and click the XQuartz DMG icon to download "
"it."
msgstr ""

#: ../../source/installing-on-mac.rst:78
msgid ""
"Once the download has finished, open your :file:`Downloads` folder in the"
" Finder. You can open this folder by selecting :menuselection:`Go --> "
"Downloads` from the menu bar in the Finder. Double-click the XQuartz.dmg "
"file to open it."
msgstr ""

#: ../../source/installing-on-mac.rst:85
msgid ""
"A new window will appear. Double-click the XQuartz.pkg icon to launch the"
" XQuartz installer. Follow the steps and instructions in installer to "
"finish installing XQuartz."
msgstr ""

#: ../../source/installing-on-mac.rst:92
msgid ""
"Restart, shut down, or log out of your Mac before proceeding to the next "
"step."
msgstr ""

#: ../../source/installing-on-mac.rst:99
msgid "Step 2: Install Inkscape"
msgstr ""

#: ../../source/installing-on-mac.rst:101
msgid ""
"Using a web browser, go to the `Inkscape 0.92.2 downloads page "
"<https://inkscape.org/release/0.92.2/mac-os-x/>`_."
msgstr ""

#: ../../source/installing-on-mac.rst:107
msgid ""
"Click the box labelled :guilabel:`Mac OS X 10.7 Installer (xquartz)` to "
"download the DMG file, which contains the installer."
msgstr ""

#: ../../source/installing-on-mac.rst:110
msgid ""
"Once the download has finished, open your :file:`Downloads` folder in the"
" Finder. Once again, you can open this folder by selecting "
":menuselection:`Go --> Downloads` from the menu bar in the Finder. "
"Double-click the Inkscape DMG file to open it."
msgstr ""

#: ../../source/installing-on-mac.rst:126
msgid "Step 3: Setting Up Inkscape"
msgstr ""

#: ../../source/installing-on-mac.rst:136
msgid ""
"Click OK in this window, which appears the first time Inkscape is opened."
" Wait for Inkscape to open. This might take a few minutes, since Inkscape"
" is scanning all the font files in your system. The next time you open "
"Inkscape, it will not take nearly as long to show up."
msgstr ""

#: ../../source/installing-on-mac.rst:144
msgid ""
"Once Inkscape does open, its interface will appear inside XQuartz. When "
"Inkscape is running, you will see the name \"Inkscape\" in the title bar "
"of your window, but the menu bar will show the name \"XQuartz\" ."
msgstr ""

#: ../../source/installing-on-mac.rst:151
msgid ""
"With Inkscape open, select the XQuartz preferences by selecting "
":menuselection:`XQuartz --> Preferences...` from the menu bar."
msgstr ""

#: ../../source/installing-on-mac.rst:157
msgid "Click the Input tab, and configure its settings as shown here."
msgstr ""

#: ../../source/installing-on-mac.rst:162
msgid "Click the Pasteboard tab, and configure its settings as shown here."
msgstr ""

#: ../../source/installing-on-mac.rst:167
msgid "Close Preferences. You are now ready to use Inkscape."
msgstr ""

#: ../../source/installing-on-mac.rst:169
msgid ""
"Note that when using Inkscape version 0.92 with XQuartz, its keyboard "
"shortcuts use the **control** key, rather than the usual command key."
msgstr ""

#: ../../source/installing-on-mac.rst:174
msgid "Homebrew and MacPorts"
msgstr ""

#: ../../source/installing-on-mac.rst:176
msgid ""
"In addition to the preceding \"standard\" methods of installing Inkscape,"
" you can also build Inkscape on your Mac using Homebrew or MacPorts. "
"These methods are intended for users who prefer these environments; most "
"users should use the recommended methods above. You will need to be "
"familiar with the macOS terminal and you may need to `install Xcode "
"<https://guide.macports.org/#installing.xcode>`_ and its command line "
"developer tools in order to use these methods. Depending on the type of "
"Inkscape build, you may also need XQuartz."
msgstr ""

#: ../../source/installing-on-mac.rst:187
msgid "Using Homebrew"
msgstr ""

#: ../../source/installing-on-mac.rst:189
msgid ""
"Install Homebrew, using the instructions at `the main Homebrew site "
"<https://brew.sh>`_."
msgstr ""

#: ../../source/installing-on-mac.rst:191
msgid ""
"See the `Homebrew instructions for Inkscape 0.92.2 "
"<https://inkscape.org/release/inkscape-0.92.2/mac-os-x/homebrew/dl/>`_ "
"for the specific Homebrew formula to use."
msgstr ""

#: ../../source/installing-on-mac.rst:197
msgid "Using MacPorts"
msgstr ""

#: ../../source/installing-on-mac.rst:199
msgid ""
"Install MacPorts. Read `the guide "
"<https://guide.macports.org/#installing.macports>`_ on how to do so, "
"since there are different methods and requirements depending on your "
"operating system version."
msgstr ""

#: ../../source/installing-on-mac.rst:204
msgid ""
"See the `MacPorts instructions for Inkscape 0.92.2 "
"<https://inkscape.org/release/inkscape-0.92.2/mac-os-x/macports/dl/>`_ "
"for the specific MacPorts installation commands to use."
msgstr ""

